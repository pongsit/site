<?php
	
require_once("../system/init.php");

$role = new \pongsit\role\role();
$site = new \pongsit\site\site();

if(!($_SESSION['user']['id']==1 || $role->check('admin'))){
	$view = new \pongsit\view\view('message');
	$variables = array();
	$variables['message'] = 'คุณไม่มีสิทธิ์ใช้หน้านี้ครับ';
	echo $view->create($variables);
	exit();
}

$infos = $site->get_info();
$info = '';
if(!empty($infos)){
	foreach($infos as $key=>$value){
		if($key=='id'){continue;}
		if(empty($value)){continue;}
		switch($key){
			case 'name': $value='<div><h3>'.$value.'</h3></div>'; break;
			case 'description': $value='<div class="text-muted"><em>'.$value.'</em></div>'; break;
			case 'author': $value='<div class="lead">'.$value.'</div>'; break;
		}
		$info .= $value;
	}
}

// add views
$variables = array();
if(!empty($info)){
	$variables['info_list'] = $info;
}else{
	$variables['info_list'] = 'กรุณาเพิ่มข้อมูลครับ';
}
$variables['page-name'] = 'ตั้งค่าเว็บ';
echo $view->create($variables);
