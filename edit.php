<?php
	
require_once("../system/init.php");

$role = new \pongsit\role\role();
$user = new \pongsit\user\user();
$site = new \pongsit\site\site();

if($_SESSION['user']['id'] != 1){
	$view = new \pongsit\view\view('message');
	$variables = array();
	$variables['message'] = 'คุณไม่มีสิทธิ์ใช้หน้านี้ครับ';
	echo $view->create($variables);
	exit();
}

$infos = $site->get_info();
$info_show = '';
if(!empty($infos)){
	foreach($infos as $key=>$value){
		$key_show='';
		if($key=='id'){continue;}
		switch($key){
			case 'name': $key_show='ชื่อ:'; break;
			case 'description': $key_show='คำอธิบาย:'; break;
			case 'author': $key_show='ผู้จัดทำ:'; break;
		   	case 'type': $key_show='ประเภท:'; break;
		}
		if(empty($key_show)){ continue; }
		$variables=array();
		$variables['label']=$key_show;
		$variables['info']='<input name="'.$key.'" class="form-control text-right" type="text" value="'.$value.'">';
		$info_show .= $view->block('setting-edit-form',$variables);
	}
}else{
	$infos = array('name'=>'','description'=>'','author'=>'','type'=>'');
	foreach($infos as $key=>$value){
	   switch($key){
		   case 'name': $key_show='ชื่อ:'; break;
		   case 'description': $key_show='คำอธิบาย:'; break;
		   case 'author': $key_show='ผู้จัดทำ:'; break;
		   case 'type': $key_show='ประเภท:'; break;
	   }
	   $variables['label']=$key_show;
	   $variables['info']='<input name="'.$key.'" class="form-control text-right" type="text" value="'.$value.'">';
	   $info_show .= $view->block('setting-edit-form',$variables);
	}
}


// add view
$variables=array();
$variables['notification']='';
$variables['h1'] = $view->block('h1',array('message'=>'แบบฟอร์มแก้ข้อมูล Website','css'=>'col-7 text-center'));

if($_POST){
	unset($_POST['submit']);
	foreach($_POST as $key=>$value){
		if(empty($value)){unset($_POST[$key]);}
	}
	$results = $db->query_array('select * from site where id=1;');
	if(empty($results)){
		$db->insert('site',$_POST);
	}else{
		$db->update('site',$_POST,'id=1');
	}
	header('Location:setting.php');
}
$variables['info-list']=$info_show;
echo $view->create($variables);
