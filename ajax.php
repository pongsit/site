<?php
	
require_once("../system/init.php");	
$file = new \pongsit\file\file();

/*
print_r($_FILES);
Array
(
    [file] => Array
        (
            [name] => blob
            [type] => image/jpeg
            [tmp_name] => /Applications/MAMP/tmp/php/phpV6UdcJ
            [error] => 0
            [size] => 48013
        )

)
*/


// -- for debug --
// error_log(print_r(getimagesize($_FILES['file']['tmp_name']),true));
// error_log(print_r($_POST,true));

if(empty($_FILES)){
	echo 'Error: No file found!';
	exit();
}

$type = $_FILES['file']['type'];

$ext = '';
if($type == 'image/jpeg' || $type == 'image/png'){
	if(!empty($_POST['be'])){
		$be = $_POST['be'];
	}else{
		$be = 'logo';
	}
	
	$path_infos = pathinfo($type);
	$ext = $path_infos['basename'];
	if(!file_exists($GLOBALS['path_to_root'].'app/site/img/')){
		mkdir($GLOBALS['path_to_root'].'app/site/img/',0755,true);
	}
	$file->delete_all_with_file_name($GLOBALS['path_to_root'].'app/site/img/'.$be);
	rename($_FILES['file']['tmp_name'], $GLOBALS['path_to_root'].'app/site/img/'.$be.'.'.$ext);
	chmod($GLOBALS['path_to_root'].'app/site/img/'.$be.'.'.$ext,0644);
	echo 1;
}else{
	echo '.jpg or .png file only!';
}


