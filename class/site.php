<?php
namespace pongsit\site;

class site extends \pongsit\model\model{
	
	public function __construct() 
    {
	     $this->db = $GLOBALS['db'];
	     
    }
    
	function get_info($id=1){
		$row = $this->db->query_array0("SELECT * FROM site;");
		return $row;
	}
	
	function get_img($name){
		if(file_exists($GLOBALS['path_to_root'].'app/site/img/'.$name.'.png')){
			return $GLOBALS['path_to_root'].'app/site/img/'.$name.'.png';
		}else if(file_exists($GLOBALS['path_to_root'].'app/site/img/'.$name.'.jpeg')){
			return $GLOBALS['path_to_root'].'app/site/img/'.$name.'.jpeg';
		}else if(file_exists($GLOBALS['path_to_vendor'].'pongsit/site/img/'.$name.'.png')){
			return $GLOBALS['path_to_vendor'].'pongsit/site/img/'.$name.'.png';
		}else if(file_exists($GLOBALS['path_to_vendor'].'pongsit/site/img/'.$name.'.jpeg')){
			return $GLOBALS['path_to_vendor'].'pongsit/site/img/'.$name.'.jpeg';
		}
	}
}