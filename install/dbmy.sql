CREATE TABLE `site` (
  `id` INT AUTO_INCREMENT primary key NOT NULL,
  `name` text,
  `description` text,
  `author` text,
  `type` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;